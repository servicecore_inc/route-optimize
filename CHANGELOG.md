# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 1.0.0.
### Added
- Adding a Route4Me provider implementation
- Adding an ORTools python library implementation
- Adding a repo for route optimization

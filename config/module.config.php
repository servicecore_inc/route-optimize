<?php

namespace ServiceCore\RouteOptimize;

use ServiceCore\RouteOptimize\Context\Factory\OptimizeHere as OptimizeHereFactory;
use ServiceCore\RouteOptimize\Context\Factory\OptimizeRoute4Me as OptimizeRoute4MeFactory;
use ServiceCore\RouteOptimize\Context\OptimizeHere;
use ServiceCore\RouteOptimize\Context\OptimizeRoute4Me;
use ServiceCore\RouteOptimize\Service\Factory\Optimize as OptimizeHelperFactory;
use ServiceCore\RouteOptimize\Service\Optimize as OptimizeHelper;

return [
    'route-optimize' => [
        'providers' => [
            OptimizeHelper::ROUTE4ME => [
                'apiKey' => null
            ],
            OptimizeHelper::HERE     => [
                'apiKey' => null
            ],
        ]
    ],

    'service_manager' => [
        'factories' => [
            OptimizeHelper::class   => OptimizeHelperFactory::class,
            OptimizeRoute4Me::class => OptimizeRoute4MeFactory::class,
            OptimizeHere::class     => OptimizeHereFactory::class,
        ]
    ]
];

<?php

namespace ServiceCore\RouteOptimize\Service;

use InvalidArgumentException;
use ServiceCore\RouteOptimize\Context\OptimizeInterface;

class Optimize
{
    public const ORTOOLS  = 'ortools';
    public const ROUTE4ME = 'route4me';
    public const HERE     = 'here';

    /** @var array */
    private $contexts;

    public function __construct(array $contexts)
    {
        if (($missing = \array_diff(self::getAvailableOptimizers(), \array_keys($contexts))) !== []) {
            throw new InvalidArgumentException('Missing optimize implementations: ' . implode(', ', $missing));
        }

        foreach ($contexts as $context) {
            if (!$context instanceof OptimizeInterface) {
                throw new InvalidArgumentException(
                    'The \'' . \get_class($context) . '\' context must implement \'' . OptimizeInterface::class . '\''
                );
            }
        }

        $this->contexts = $contexts;
    }

    public static function getAvailableOptimizers(): array
    {
        return [
            self::ORTOOLS,
            self::ROUTE4ME,
            self::HERE
        ];
    }

    public function getOptimizer(string $optimizer): OptimizeInterface
    {
        switch ($optimizer) {
            case self::ORTOOLS:
                return $this->contexts[self::ORTOOLS];
            case self::ROUTE4ME:
                return $this->contexts[self::ROUTE4ME];
            case self::HERE:
                return $this->contexts[self::HERE];
            default:
                throw new InvalidArgumentException(
                    'Invalid optimizer selected. Available optimizers: '
                    . \implode(', ', self::getAvailableOptimizers())
                );
        }
    }
}

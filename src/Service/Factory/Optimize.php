<?php

namespace ServiceCore\RouteOptimize\Service\Factory;

use Interop\Container\ContainerInterface;
use ServiceCore\RouteOptimize\Context\OptimizeHere;
use ServiceCore\RouteOptimize\Context\OptimizeORTools;
use ServiceCore\RouteOptimize\Context\OptimizeRoute4Me;
use ServiceCore\RouteOptimize\Service\Optimize as OptimizeHelper;
use Zend\ServiceManager\Factory\FactoryInterface;

class Optimize implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): OptimizeHelper
    {
        return new OptimizeHelper([
            OptimizeHelper::ORTOOLS  => new OptimizeORTools(),
            OptimizeHelper::ROUTE4ME => $container->get(OptimizeRoute4Me::class),
            OptimizeHelper::HERE     => $container->get(OptimizeHere::class)
        ]);
    }
}

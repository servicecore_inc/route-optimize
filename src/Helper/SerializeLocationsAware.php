<?php

namespace ServiceCore\RouteOptimize\Helper;

use ServiceCore\RouteOptimize\Collection\Location as LocationCollection;
use ServiceCore\RouteOptimize\Data\Location;

trait SerializeLocationsAware
{
    public static function serializeLocations(LocationCollection $locations): array
    {
        return \array_map(function (Location $location, $key) {
            return [
                'key'       => $key,
                'id'        => $location->getId(),
                'latitude'  => $location->getLatitude(),
                'longitude' => $location->getLongitude()
            ];
        }, $locations->get(), $locations->getKeys());
    }

    protected function unserializeLocations(array $locationsData): LocationCollection
    {
        $collection = new LocationCollection();

        foreach ($locationsData as $item) {
            $collection->add($item['key'], new class($item) implements Location
            {
                private $id;

                private $latitude;

                private $longitude;

                public function __construct($serialized)
                {
                    $this->id        = $serialized['id'];
                    $this->latitude  = $serialized['latitude'];
                    $this->longitude = $serialized['longitude'];
                }

                public function getId(): int
                {
                    return $this->id;
                }

                public function getLatitude(): ?float
                {
                    return $this->latitude;
                }

                public function getLongitude(): ?float
                {
                    return $this->longitude;
                }
            });
        }

        return $collection;
    }
}

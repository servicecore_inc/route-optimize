<?php

namespace ServiceCore\RouteOptimize\Adapter;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use InvalidArgumentException;
use RuntimeException;
use ServiceCore\RouteOptimize\Collection\Location as LocationCollection;
use ServiceCore\RouteOptimize\Data\Location;
use ServiceCore\RouteOptimize\Exception\MissingApiKey;

class Here
{
    private const WAYPOINT_SEQUENCE_ENDPOINT = 'https://wse.ls.hereapi.com/2/findsequence.json';

    /**
     * @var null|string
     */
    private $apiKey;

    /**
     * @var Client
     */
    private $client;

    public function __construct(?string $apiKey = null)
    {
        $this->apiKey = $apiKey;
        $this->client = new Client();
    }

    public function optimizeSequence(LocationCollection $destinations): array
    {
        if ($this->apiKey === null) {
            throw new MissingApiKey('here.com');
        }

        if ($destinations->count() < 3) {
            throw new InvalidArgumentException('You must provide at least 3 destinations');
        }

        $count       = 0;
        $queryParams = [
            'apiKey' => $this->apiKey,
            'mode'   => 'fastest;truck'
        ];

        foreach ($destinations->get() as $key => $destination) {
            if ($count === 0) {
                $queryParams['start'] = $this->stringifyLocation($key, $destination);
            } else {
                $queryParams['destination' . $count] = $this->stringifyLocation($key, $destination);
            }
            $count++;
        }

        try {
            $response = $this->client->get(self::WAYPOINT_SEQUENCE_ENDPOINT . '?' . http_build_query($queryParams));

            return \json_decode($response->getBody()->getContents(), true);
        } catch (ClientException $e) {
            throw new RuntimeException('There was a problem optimizing the route', 0, $e);
        }
    }

    private function stringifyLocation(string $key, Location $destination): string
    {
        return \sprintf('%s;%s,%s', $key, $destination->getLatitude(), $destination->getLongitude());
    }
}

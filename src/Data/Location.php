<?php

namespace ServiceCore\RouteOptimize\Data;

interface Location
{
    public function getId(): int;

    public function getLatitude(): ?float;

    public function getLongitude(): ?float;
}

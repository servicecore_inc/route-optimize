<?php

namespace ServiceCore\RouteOptimize\Exception;

use Exception;

abstract class AbstractOptimize extends Exception
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}

<?php

namespace ServiceCore\RouteOptimize\Exception;

class MissingApiKey extends AbstractOptimize
{
    public function __construct(string $optimizer)
    {
        parent::__construct(\sprintf('Missing API Key for %s', $optimizer));
    }
}

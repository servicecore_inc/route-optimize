<?php

namespace ServiceCore\RouteOptimize\Collection;

use Countable;
use InvalidArgumentException;
use ServiceCore\RouteOptimize\Data\Location as LocationInterface;

class Location implements Countable
{
    /**
     * @var LocationInterface[]
     */
    private $locations = [];

    /**
     * @var int
     */
    private $distance = 0;

    /**
     * @var int
     */
    private $authorityId;

    public function __construct(array $locations = [])
    {
        foreach ($locations as $location) {
            if (!$location instanceof LocationInterface) {
                throw new InvalidArgumentException('All Locations must implement ' . LocationInterface::class);
            }

            $this->locations[$location->getId()] = $location;
        }
    }

    /**
     * @param string|int        $key
     * @param LocationInterface $location
     *
     * @return Location
     */
    public function add($key, LocationInterface $location): self
    {
        $this->locations[$key] = $location;

        return $this;
    }

    /**
     * @param string|int|null $key
     *
     * @return LocationInterface|LocationInterface[]
     */
    public function get($key = null)
    {
        if ($key) {
            if (!\array_key_exists($key, $this->locations)) {
                throw new InvalidArgumentException('Could not find a location with key: ' . $key);
            }

            return $this->locations[$key];
        }

        return $this->locations;
    }

    public function getKeys(): array
    {
        return \array_keys($this->locations);
    }

    public function reset(): self
    {
        $this->locations = [];

        return $this;
    }

    public function count(): int
    {
        return \count($this->locations);
    }

    public function first(): LocationInterface
    {
        return \reset($this->locations);
    }

    public function last(): LocationInterface
    {
        return \end($this->locations);
    }

    public function slice(int $offset, ?int $length = null): array
    {
        return \array_slice($this->locations, $offset, $length, true);
    }

    public function getDistance(): int
    {
        return $this->distance;
    }

    public function setDistance(int $distance): Location
    {
        $this->distance = $distance;

        return $this;
    }

    public function getAuthorityId(): int
    {
        return $this->authorityId;
    }

    public function setAuthorityId(int $authorityId): Location
    {
        $this->authorityId = $authorityId;

        return $this;
    }
}

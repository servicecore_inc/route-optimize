<?php

namespace ServiceCore\RouteOptimize\Context;

use ServiceCore\RouteOptimize\Adapter\Here as HereAdapter;
use ServiceCore\RouteOptimize\Collection\Location as LocationCollection;
use ServiceCore\RouteOptimize\Exception\MissingApiKey;

class OptimizeHere implements OptimizeInterface
{
    /**
     * @var HereAdapter
     */
    private $adapter;

    public function __construct(?string $apiKey = null)
    {
        $this->adapter = new HereAdapter($apiKey);
    }

    public function __invoke(LocationCollection $locations): LocationCollection
    {
        $response         = $this->adapter->optimizeSequence($locations);
        $orderedSequence  = $response['results'][0]['waypoints'];
        $orderedLocations = new LocationCollection();

        foreach ($orderedSequence as $destination) {
            $orderedLocations->add($destination['id'], $locations->get($destination['id']));
        }

        return $orderedLocations;
    }
}

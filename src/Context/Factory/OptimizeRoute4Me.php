<?php

namespace ServiceCore\RouteOptimize\Context\Factory;

use Interop\Container\ContainerInterface;
use ServiceCore\RouteOptimize\Context\OptimizeRoute4Me as OptimizeRoute4MeContext;
use ServiceCore\RouteOptimize\Service\Optimize as OptimizeHelper;
use Zend\ServiceManager\Factory\FactoryInterface;

class OptimizeRoute4Me implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): OptimizeRoute4MeContext
    {
        $config = $container->get('config')['route-optimize'];

        return new OptimizeRoute4MeContext($config['providers'][OptimizeHelper::ROUTE4ME]['apiKey']);
    }
}

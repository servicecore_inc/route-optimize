<?php

namespace ServiceCore\RouteOptimize\Context\Factory;

use Interop\Container\ContainerInterface;
use ServiceCore\RouteOptimize\Context\OptimizeHere as OptimizeHereContext;
use ServiceCore\RouteOptimize\Service\Optimize as OptimizeHelper;
use Zend\ServiceManager\Factory\FactoryInterface;

class OptimizeHere implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): OptimizeHereContext
    {
        $config = $container->get('config')['route-optimize'];

        return new OptimizeHereContext($config['providers'][OptimizeHelper::HERE]['apiKey']);
    }
}

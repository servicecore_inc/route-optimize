<?php

namespace ServiceCore\RouteOptimize\Context;

use InvalidArgumentException;
use ServiceCore\RouteOptimize\Collection\Location as LocationCollection;
use ServiceCore\RouteOptimize\Data\Location as LocationInterface;
use ServiceCore\RouteOptimize\Exception\OptimizationFailed;

class OptimizeORTools implements OptimizeInterface
{
    public function __invoke(LocationCollection $locations): LocationCollection
    {
        $locationArg = $this->convertCollectionToArray($locations);

        if (!$arguments = \json_encode(['locations' => $locationArg])) {
            throw new InvalidArgumentException('Could not JSON encode `locations`');
        }

        $command = 'python ' . \dirname(__DIR__) . '/../bin/optimize_distance.py'
            . ' --json ' . '\'' . $arguments . '\'';

        \exec("{$command}", $response, $exitCode);

        if ($exitCode === 1) {
            throw new OptimizationFailed('ORTools has encountered an error attempting to optimize.');
        }

        if (!$result = \json_decode($response[0], true)) {
            throw new OptimizationFailed('ORTools has encountered an error trying to decode json results.');
        }

        return $this->convertArrayToCollection($result['body'], $locations);
    }

    private function convertCollectionToArray(LocationCollection $locations): array
    {
        $result = [];

        /** @var LocationInterface $location */
        foreach ($locations->get() as $key => $location) {
            $result[][$key] = [
                (float)$location->getLatitude(),
                (float)$location->getLongitude()
            ];
        }

        return $result;
    }

    private function convertArrayToCollection(array $results, LocationCollection $locations): LocationCollection
    {
        $sortedLocations = new LocationCollection();

        foreach ($results['optimalRoute'] as $location) {
            $sortedLocations->add($location, $locations->get($location));
        }

        $sortedLocations->setDistance($results['totalDistance']);

        return $sortedLocations;
    }
}

<?php

namespace ServiceCore\RouteOptimize\Context;

use ServiceCore\RouteOptimize\Collection\Location as LocationCollection;

interface OptimizeInterface
{
    /**
     * @param LocationCollection $locations
     *
     * @return LocationCollection
     */
    public function __invoke(LocationCollection $locations): LocationCollection;
}

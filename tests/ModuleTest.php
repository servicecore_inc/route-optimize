<?php

namespace ServiceCore\RouteOptimize\Test;

use PHPUnit\Framework\TestCase;
use ServiceCore\RouteOptimize\Module;
use ServiceCore\RouteOptimize\Service\Optimize as OptimizeHelper;

class ModuleTest extends TestCase
{
    public function testGetConfig(): void
    {
        $module = new Module();
        $config = $module->getConfig();

        self::assertArrayHasKey('route-optimize', $config);
        self::assertIsArray($config['route-optimize']);
        self::assertArrayHasKey('providers', $config['route-optimize']);
        self::assertIsArray($config['route-optimize']['providers']);
        self::assertArrayHasKey(OptimizeHelper::ROUTE4ME, $config['route-optimize']['providers']);
        self::assertArrayHasKey('service_manager', $config);
    }
}

<?php

namespace ServiceCore\RouteOptimize\Test\Adapter;

use PHPUnit\Framework\TestCase;
use RuntimeException;
use ServiceCore\RouteOptimize\Adapter\Here;
use ServiceCore\RouteOptimize\Collection\Location as LocationCollection;
use ServiceCore\RouteOptimize\Data\Location;

class HereTest extends TestCase
{
    public function testAdapter(): void
    {
        $adapter = new Here('');

        $destinations = new LocationCollection([
            $this->generateCoordinates(0),
            $this->generateCoordinates(1),
            $this->generateCoordinates(2),
            $this->generateCoordinates(3),
        ]);

        $this->expectException(RuntimeException::class);
        $adapter->optimizeSequence($destinations);
    }

    private function generateCoordinates(int $id): Location
    {
        return new class($id) implements Location {
            private $id;

            public function __construct(int $id)
            {
                $this->id = $id;
            }

            public function getId(): int
            {
                return $this->id;
            }

            public function getLatitude(): ?float
            {
                return (\random_int(5000, 10000) / 10000) + 39;
            }

            public function getLongitude(): ?float
            {
                return (\random_int(5000, 10000) / 10000) - 105;
            }
        };
    }
}

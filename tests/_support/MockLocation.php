<?php

namespace ServiceCore\RouteOptimize\Test\Support;

use ServiceCore\RouteOptimize\Data\Location;

class MockLocation implements Location
{
    /** @var int */
    private $id;

    /** @var float */
    private $latitude;

    /** @var float */
    private $longitude;

    public function __construct(int $id, float $latitude = 0, float $longitude = 0)
    {
        $this->id = $id;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }
}
